import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import User from "App/Models/User";
import UserValidator from "App/Validators/UserValidator";
import { schema, rules } from "@ioc:Adonis/Core/Validator";
import Mail from "@ioc:Adonis/Addons/Mail";

export default class AuthController {
  public async store({ request, response, auth }: HttpContextContract) {
    try {
      let data = await request.validate(UserValidator);
      const otp_code = Math.floor(100000 + Math.random() * 900000).toString();
      const user = await User.create({
        name: data.name,
        email: data.email,
        password: data.password,
        otp_code: otp_code,
      });
      const token = await auth
        .use("api")
        .attempt(request.input("email"), request.input("password"));

      await Mail.send((message) => {
        message
          .from("tugas_akhir_sanber@adonisjs.com")
          .to(user.email)
          .subject("Kode Verifikasi Tugas Akhir Adonisjs")
          .htmlView("emails/otp_verification", { code: otp_code });
      });
      return response.json({
        message: `berhasil mendaftar sebagai user, silahkan lakukan verifikasi dengan kode yang telah dikirimkan ke ${user.email}`,
        token: `${token.token}`,
        data: { name: user.name, email: user.email, role: user.role },
      });
    } catch (error) {
      return response.json({ message: error.messages });
    }
  }

  public async otp_confirmation({ request, response, auth }: HttpContextContract) {
    try {
      const user_id = auth.user?.id;
      const otpSchema = schema.create({
        otp_code: schema.string({}, [rules.maxLength(6), rules.minLength(6)]),
      });
      const payload = await request.validate({ schema: otpSchema });

      const user = await User.find(user_id);
      if (user != null) {
        if (user.otp_code === payload.otp_code) {
          user.is_verified = true;
          await user.save();

          response.json({ message: "berhasil terverifikasi" });
        }else{
          response.json({message:"kode otp salah"})
        }
      }
    } catch (error) {
      response.json({ message: error.message });
    }
  }

  public async login({ request, response, auth }: HttpContextContract) {
    try {
      const loginSchema = schema.create({
        email: schema.string(),
        password: schema.string(),
      });
      await request.validate({ schema: loginSchema });
      const token = await auth
        .use("api")
        .attempt(request.input("email"), request.input("password"));

      return response.ok({ message: "Login berhasil", token });
    } catch (error) {
      return response.json({ message:'error', error: error.messages });
    }
  }
}
