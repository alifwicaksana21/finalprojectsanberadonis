import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Database from "@ioc:Adonis/Lucid/Database";
import Booking from "App/Models/Booking";
import Field from "App/Models/Field";
import User from "App/Models/User";
import BookingValidator from "App/Validators/BookingValidator";

export default class BookingsController {
  public async index({ response }: HttpContextContract) {
    const bookings = await Booking.all();

    response.json({
      message: "berhasil mendapatkan data bookings",
      data: bookings,
    });
  }

  public async store({ request, params, response, auth }: HttpContextContract) {
    const user_id = auth.user?.id;
    const field_id = params.id;
    try {
      const data = await request.validate(BookingValidator);
      const booking = new Booking();
      await booking.fill({ ...request.body(), user_id, field_id }).save();

      response.json({
        message: "berhasil melakukan booking",
        data: { user_id, field_id, ...data },
      });
    } catch (error) {
      response.json({ message: error.message });
    }
  }

  public async show({ params, response }: HttpContextContract) {
    const id = params.id;
    const booking = await Booking.find(id);

    if (booking != null) {
      let list_player = [] as any
      const players = await Database.query().from('user_into_booking').where('booking_id', id)
      for(let i=0;i<players.length;i++){
        const user = await User.find(players[i].user_id)
        list_player.push(user?.name)
      }

      return response.json({
        message: `berhasil mengambil data booking dengan id ${id}`,
        data: {booking, list_pemain:list_player},
      });
    } else {
      return response.json({
        message: `tidak ada data booking dengan id ${id}`,
      });
    }
  }

  public async join({ auth, params, response }: HttpContextContract) {
    const user_id = auth.user?.id;
    const booking_id = params.id;

    const booking = Booking.find(booking_id);

    if (booking === null) {
      return response.json({
        message: `tidak ada kode booking dengan id ${booking_id}`,
      });
    } else {
      await Database.insertQuery().table("user_into_booking").insert({
        user_id,
        booking_id,
      });
      return response.json({
        message: `berhasil join ke dalam booking dengan id ${booking_id}`,
      });
    }
  }

  public async unjoin({ auth, params, response }: HttpContextContract) {
    const user_id = auth.user?.id;
    const booking_id = params.id;
    console.log(user_id);
    console.log(booking_id);

    const booking = Booking.find(booking_id);

    if (user_id != null) {
      if (booking === null) {
        return response.json({
          message: `tidak ada kode booking dengan id ${booking_id}`,
        });
      } else {
        await Database.query()
          .from("user_into_booking")
          .where("user_id", user_id)
          .where("booking_id", booking_id)
          .delete();
        return response.json({ message: "berhasil unjoin" });
      }
    }
  }

  public async schedules({ auth, response }: HttpContextContract) {
    const user_id = auth.user?.id;
    if (user_id != null) {
      const schedules = await Database.query()
        .from("user_into_booking")
        .where("user_id", user_id);

      if (schedules.length > 0) {
        let data = [] as any;
        for (let i = 0; i < schedules.length; i++) {
          let booking = await Booking.find(schedules[i].booking_id);
          let field = await Field.find(booking?.field_id)
          data.push({
            field_name: field?.name,
            field_type: field?.type,
            venue_id: field?.venue_id,
            play_date_start: booking?.play_date_start,
            play_date_end: booking?.play_date_end
          });
        }

        return response.json({
          message: `berhasil mendapatkan data jadwal untuk user dengan id ${user_id}`,
          data: data,
        });
      } else {
        return response.json({
          message: `tidak ada data untuk user dengan id ${user_id}`,
        });
      }
    }
  }
}
