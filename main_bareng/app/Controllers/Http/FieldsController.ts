import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { schema } from "@ioc:Adonis/Core/Validator";
import Booking from "App/Models/Booking";
import Field from "App/Models/Field";
import Venue from "App/Models/Venue";
import FieldValidator from "App/Validators/FieldValidator";

export default class FieldsController {
  public async index({ response }: HttpContextContract) {
    const fields = await Field.all();

    response.status(200).json(fields);
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const data = await request.validate(FieldValidator);

      const venue = Venue.find(data.venue_id);

      if (venue !== null) {
        const field = new Field();
        await field.fill(data).save();

        response
          .status(201)
          .json({ message: "berhasil menambahkan data field baru" });
      } else {
        response
          .status(404)
          .json({ message: `there is no venue with id : ${data.venue_id}` });
      }
    } catch (error) {
      response.json({ message: error.message });
    }
  }

  public async show({ request, response }: HttpContextContract) {
    const params = request.params();
    const id = params.id;

    let data = await Field.find(id);

    if (data === null) {
      return response
        .status(404)
        .json({ message: `tidak ada field dengan id : ${id}` });
    }

    const venue = await Venue.find(data.id);
    const bookings = await Booking.query().where("field_id", id);

    return response
      .status(201)
      .json({
        message: "berhasil get data booking",
        data: { name: data.name, type: data.type, venue, bookings },
      });
  }

  public async update({ request, response }: HttpContextContract) {
    const params = request.params();
    const id = params.id;

    const newFieldSchema = schema.create({
      name: schema.string({}),
      type: schema.enum(["futsal", "mini soccer", "basketball"]),
      venue_id: schema.number(),
    });

    try {
      const payload = await request.validate({
        schema: newFieldSchema,
      });

      const venue = await Venue.find(payload.venue_id);
      if (venue === null) {
        return response.json({
          message: `there is no venue_id : ${payload.venue_id}`,
        });
      }

      const field = await Field.findOrFail(id);
      field.name = payload.name;
      field.type = payload.type;
      field.venue_id = payload.venue_id;
      field.save();

      // await Database
      //     .query()
      //     .update({name:payload.nama, type:payload.type, venue_id:payload.venue_id})
      //     .from('fields')
      //     .where('id', id)

      response.status(201).json(payload);
    } catch (error) {
      response.badRequest(error.messages);
    }
  }

  public async delete({ request, response }: HttpContextContract) {
    const params = request.params();
    const id = params.id;

    const field = await Field.findOrFail(id);
    field.delete();

    // await Database
    //     .query()
    //     .from('fields')
    //     .delete()
    //     .where('id', id)

    response.status(201).json({ message: "success deleted" });
  }
}
