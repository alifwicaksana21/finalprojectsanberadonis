import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Venue from "App/Models/Venue";
import VenueValidator from "App/Validators/VenueValidator";

export default class VenuesController {
  public async index({ response }: HttpContextContract) {
    try {
      const venues = await Venue.all();

      return response.json({
        message: "sukses mengambil list data venues",
        data: venues,
      });
    } catch (error) {
      return response.badRequest({ message: error.message });
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const data = await request.validate(VenueValidator);
      const venue = await Venue.create(data);

      return response.json({
        message: "sukses menambahkan venue",
        data: venue,
      });
    } catch (error) {
      return response.badRequest({ message: error.message });
    }
  }

  public async show({ params, response }: HttpContextContract) {
    try {
      const venue = await Venue.find(params.id);
      if (venue === null) {
        return response
          .status(404)
          .json({ message: `tidak ada data venue dengan id ${params.id}` });
      } else {
        return response.json({
          message: `sukses mengambil data venue dengan id ${params.id}`,
          data: venue,
        });
      }
    } catch (error) {
      return response.badRequest({ message: error.message });
    }
  }

  public async update({
    params,
    request,
    response,
  }: HttpContextContract) {
    const id = params.id;

    try {
      const data = await request.validate(VenueValidator);
      const venue = await Venue.find(id);
      if (venue != null) {
        venue.name = data.name;
        venue.phone = data.phone;
        venue.address = data.address;
        venue.save();
        return response.json({
          message: "sukses mengupdate data venue",
          data: venue,
        });
      } else {
        return response.json({ message: `tidak ada venue dengan id ${id}` });
      }
    } catch (error) {
      return response.badRequest({ message: error.message });
    }
  }
}
