import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class OwnerRole {
  public async handle ({auth, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    if(auth.user?.role === 'owner' || auth.user?.role === 'admin'){
      await next()
    }else{
      return response.unauthorized({error: 'anda tidak memiliki hak untuk mengakses data ini'})
    }
  }
}
