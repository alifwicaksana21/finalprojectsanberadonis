import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Verify {
  public async handle ({auth, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    const isVerified = auth.user?.is_verified
    if(isVerified){
      await next()
    }else{
      return response.unauthorized({error:`akun anda belum diverifikasi, mohon lakukan verifikasi dengan kode otp yang telah dikirimkan ke email anda ${auth.user?.email}`})
    }
  }
}
