import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'

export default class UserSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await User.create(
      {
        name: 'Alif Wicaksana Ramadhan',
        password: 'adminalif',
        email: 'alifwicaksana21@gmail.com',
        role: 'admin',
        is_verified: true
      }
    )
  }
}
