/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Mail from "@ioc:Adonis/Addons/Mail";
import Route from "@ioc:Adonis/Core/Route";

Route.get("/", async ({ response }) => {
  await Mail.send((message) => {
    message
      .from("alif@projekbareng.com")
      .to("bmss@gmail.com")
      .subject("Wlcome Onboard")
      .htmlView("emails/otp_verification", { code: "1242123" });
  });
  return response.send("OK");
});

Route.post("/api/v1/register", "AuthController.store");
Route.post("/api/v1/login", "AuthController.login");

Route.group(() => {
  Route.post("/api/v1/otp-confirmation", "AuthController.otp_confirmation");

  Route.group(() => {

    Route.group(() => {
      Route.get("/api/v1/venues", "VenuesController.index");
      Route.post("/api/v1/venues", "VenuesController.store");
      Route.get("/api/v1/venues/:id", "VenuesController.show");
      Route.post("/api/v1/venues/:id/bookings", "BookingsController.store");
      Route.put("/api/v1/venue/:id", "VenuesController.update");
    }).middleware("owner");

    Route.group(() => {
      Route.get("/api/v1/bookings", "BookingsController.index");
      Route.get("/api/v1/bookings/:id", "BookingsController.show");
      Route.put("/api/v1/bookings/:id/join", "BookingsController.join");
      Route.put("/api/v1/bookings/:id/unjoin", "BookingsController.unjoin");
      Route.get("/api/v1/schedules", "BookingsController.schedules");
    }).middleware("user");

  }).middleware('verify');

  Route.post("api/v1/fields", "FieldsController.store");
}).middleware("auth");
